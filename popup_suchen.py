"""
Popup Suche für pystunden.
"""
from tkinter import *
from tkinter.ttk import *
from tkinter.simpledialog import Dialog
import datetime
import re
import os


class EintragSuchen(Dialog):
    """
    Popupfenster Suche.
    """      
    def __init__(self, master=None):
        # Setzt result None
        self.result = None
        
        self.master = master
        if os.name == 'nt':
            self.master.iconbitmap("pystunden.ico")
        self.master.title("Eintrag suchen")
        
        self.l1 = Label(self.master, text="Startdatum (JJJJ-MM-TT):", width=22, 
                        anchor=E)
        self.l1.grid(row=0, sticky=W+E, padx=5, pady=5)
        self.l2 = Label(self.master, text="Enddatum (JJJJ-MM-TT):", width=22, 
                        anchor=E)
        self.l2.grid(row=3, sticky=W+E, padx=5, pady=5)        
       
        self.l3 = Label(self.master, text="Projekt:", width=22, anchor=E)
        self.l3.grid(row=6, sticky=W+E, padx=5, pady=5)
          
        
        self.startdatum_error = Label(self.master)
        self.startdatum_error.grid(row=0, column=1, padx=5, pady=5, sticky=W)
        self.startdatum = Entry(self.startdatum_error, width=20)
        self.startdatum.grid(row=0, column=0, padx=5, pady=5)

        self.searchstart = IntVar(self.master)
        self.searchstart.set(1)        
        self.check_startdatum = Checkbutton(self.master, 
                                            text="In Suche einbeziehen?", 
                                            variable=self.searchstart,
                                            onvalue=1, offvalue=0)
        self.check_startdatum.grid(row=1, column=1, padx=5, sticky=W)
        
        self.space1 = Label(self.master)
        self.space1.grid(row=2, column=1)
                
        self.enddatum_error = Label(self.master)
        self.enddatum_error.grid(row=3, column=1, padx=5, pady=5, sticky=W)
        self.enddatum = Entry(self.enddatum_error, width=20)
        self.enddatum.grid(row=0, column=0, padx=5, pady=5)

        self.searchend = IntVar(self.master)
        self.searchend.set(1)        
        self.check_enddatum = Checkbutton(self.master, 
                                            text="In Suche einbeziehen?", 
                                            variable=self.searchend,
                                            onvalue=1, offvalue=0)
        self.check_enddatum.grid(row=4, column=1, padx=5, sticky=W)

        self.space2 = Label(self.master)
        self.space2.grid(row=5, column=1)
        
        self.projekt_error = Label(self.master)
        self.projekt_error.grid(row=6, column=1, padx=5, pady=5, sticky=W)
        self.projekt = Entry(self.projekt_error, width=40)
        self.projekt.grid(row=0, column=0, padx=5, pady=5)

        self.searchprojekt = IntVar(self.master)
        self.searchprojekt.set(0)        
        self.check_projekt = Checkbutton(self.master, 
                                            text="In Suche einbeziehen?", 
                                            variable=self.searchprojekt,
                                            onvalue=1, offvalue=0)
        self.check_projekt.grid(row=7, column=1, padx=5, sticky=W)

        self.button = Button(self.master, text="Suchen", width=20, 
                             command=self.save)
        self.button.grid(row=8, column=0, padx=5, pady=5)        
             
        self.button = Button(self.master, text="Beenden", width=20,
                             command=self.master.destroy)
        self.button.grid(row=8, column=1, sticky=E, padx=5, pady=5)        
        # Populiert Start- und Enddatum
        now = datetime.datetime.today()
        self.startdatum.insert(0, "{}-{:02d}-01".format(now.year, now.month))       
        self.enddatum.insert(0, "{}-{:02d}-31".format(now.year, now.month))        
        # Bindings
        self.master.bind("<Return>", self.save)
        # Verhindert Größenänderungen
        self.master.update()
        self.master.minsize(self.master.winfo_width(), 
                            self.master.winfo_height())
        self.master.maxsize(self.master.winfo_width(), 
                            self.master.winfo_height())        

    def save(self, event=None):
        """
        Validiert alle Eingaben und setzt die Variable result als Ergebnis.
        """
        startdatum = self.startdatum.get()
        enddatum = self.enddatum.get()
        projekt = self.projekt.get()
        
        searchstart = self.searchstart.get()
        searchend = self.searchend.get()
        searchprojekt = self.searchprojekt.get()
        
        validated_startdatum = None
        validated_enddatum = None
        validated_projekt = None
         
        if searchprojekt:
            try:
                validated_projekt = self.validate_projekt(projekt)
            except:
                    self.projekt_error["background"] = "red"
                    self.projekt.focus_set()
        else:
            validated_projekt = "None"

        if searchend:
            try:
                validated_enddatum = self.validate_datum(enddatum)
            except:
                    self.enddatum_error["background"] = "red"
                    self.enddatum.focus_set()
        else:
            validated_enddatum = "None"

        if searchstart:
            try:
                validated_startdatum = self.validate_datum(startdatum)
            except:
                    self.startdatum_error["background"] = "red"
                    self.startdatum.focus_set()
        else:
            validated_startdatum = "None"
          

        if validated_startdatum and searchstart:
            self.startdatum_error["background"] = "green"
            
        if validated_enddatum and searchend :
            self.enddatum_error["background"] = "green"            

        if validated_projekt and searchprojekt:    
            self.projekt_error["background"] = "green"
                
        if validated_startdatum and validated_enddatum and validated_projekt:
            if validated_startdatum <= validated_enddatum:
                self.result = (validated_startdatum, validated_enddatum, 
                               validated_projekt)    
                self.master.destroy()
            else:
                self.enddatum_error["background"] = "red"    
                self.startdatum_error["background"] = "red"
                self.startdatum.focus_set()
           
    def validate_datum(self, datum):
        """
        Valiediert das Start- und Enddatum im Format JJJJ-MM-TT
        """
        datum_pattern = \
        r"^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$"
        regex = re.compile(datum_pattern)
        valid_date = regex.match(datum)

        if valid_date:
            return datum
        else:
            raise ValueError

    def validate_projekt(self, projekt):
        """
        Valiediert Projekt auf "nicht leer".
        """
        if projekt.strip():
            return projekt
        else:
            raise ValueError
