"""
Popup Eintrag Bearbeiten für pystunden.
"""
from tkinter import *
from tkinter.ttk import *
from tkinter.simpledialog import Dialog
import datetime
import re
import os


class EintragBearbeiten(Dialog):
    """
    Popupfenster Eintrag Bearbeiten.
    """
    def __init__(self, t_id, t_datum, t_projekt, t_startzeit, t_endzeit, 
                 t_protokoll, t_bezahlt, master=None):
        # Variablen zum Populieren der Entry Felder
        self.t_id = t_id
        self.t_datum = t_datum
        self.t_projekt = t_projekt
        self.t_startzeit = t_startzeit
        self.t_endzeit = t_endzeit
        self.t_protokoll = t_protokoll
        self.t_bezahlt = t_bezahlt
        # Setzt result None
        self.result = None
        
        self.master = master
        if os.name == 'nt':
            self.master.iconbitmap("pystunden.ico")
        self.master.title("Eintrag bearbeiten")
        
        self.l1 = Label(self.master, text="Datum (JJJJ-MM-TT): *", width=20, 
                        anchor=E)
        self.l1.grid(row=0, sticky=W+E, padx=5, pady=5)
        self.l2 = Label(self.master, text="Projekt: *", width=20, anchor=E)
        self.l2.grid(row=1, sticky=W+E, padx=5, pady=5)
        self.l3 = Label(self.master, text="Startzeit (SS:MM): *", width=20, 
                        anchor=E)
        self.l3.grid(row=2, sticky=W+E, padx=5, pady=5)
        self.l4 = Label(self.master, text="Endzeit (SS:MM): *", width=20, 
                        anchor=E)
        self.l4.grid(row=3, sticky=W+E, padx=5, pady=5)
        self.l5 = Label(self.master, text="Protokoll: *", width=20, anchor=E)
        self.l5.grid(row=4, sticky=W+E, padx=5, pady=5)
        self.l6 = Label(self.master, text="Bezahlt: *", width=20, anchor=E)
        self.l6.grid(row=5, sticky=W+E, padx=5, pady=5)          
        
        self.datum_error = Label(self.master)
        self.datum_error.grid(row=0, column=1, padx=5, pady=5, sticky=W)
        self.datum = Entry(self.datum_error, width=15)
        self.datum.grid(row=0, column=0, padx=5, pady=5)
        
        self.projekt_error = Label(self.master)
        self.projekt_error.grid(row=1, column=1, padx=5, pady=5, sticky=W)
        self.projekt = Entry(self.projekt_error, width=40)
        self.projekt.grid(row=0, column=0, padx=5, pady=5)

        self.startzeit_error = Label(self.master)
        self.startzeit_error.grid(row=2, column=1, padx=5, pady=5, sticky=W)        
        self.startzeit = Entry(self.startzeit_error, width=10)
        self.startzeit.grid(row=0, column=0, padx=5, pady=5)

        self.endzeit_error = Label(self.master)
        self.endzeit_error.grid(row=3, column=1, padx=5, pady=5, sticky=W)        
        self.endzeit = Entry(self.endzeit_error, width=10)
        self.endzeit.grid(row=0, column=0, padx=5, pady=5)

        self.protokoll_error = Label(self.master)
        self.protokoll_error.grid(row=4, column=1, padx=5, pady=5, sticky=W)        
        self.protokoll = Entry(self.protokoll_error, width=50)
        self.protokoll.grid(row=0, column=0, padx=5, pady=5)
        
        self.radio_buttons = Label(self.master)
        self.radio_buttons.grid(row=5, column=1, padx=5, pady=5, sticky=W)
        
        self.bezahlt = StringVar(self.master)
        self.bezahlt.set("Nein")
        self.bezahlt_ja = Radiobutton(self.radio_buttons, text="Ja", 
                                      variable=self.bezahlt, value=str("Ja"))
        self.bezahlt_ja.grid(row=0, column=0, padx=5, pady=5, sticky=W)
        self.bezahlt_nein = Radiobutton(self.radio_buttons, text="Nein", 
                                        variable=self.bezahlt, 
                                        value=str("Nein"))
        self.bezahlt_nein.grid(row=0, column=1, padx=5, pady=5, sticky=W)          
        
        self.button = Button(self.master, text="Speichern", width=20, 
                             command=self.save)
        self.button.grid(row=6, column=0, padx=5, pady=5)        
             
        self.button = Button(self.master, text="Beenden", width=20,
                             command=self.master.destroy)
        self.button.grid(row=6, column=1, sticky=E, padx=5, pady=5)
        # Populiert Entry Felder
        self.datum.insert(0, self.t_datum)
        self.projekt.insert(0, self.t_projekt)
        self.startzeit.insert(0, self.t_startzeit)
        self.endzeit.insert(0, self.t_endzeit)
        self.protokoll.insert(0, self.t_protokoll)
        self.bezahlt.set(self.t_bezahlt)
        # Bindings
        self.master.bind("<Return>", self.save)
        # Verhindert Größenänderungen
        self.master.update()
        self.master.minsize(self.master.winfo_width(), 
                            self.master.winfo_height())
        self.master.maxsize(self.master.winfo_width(), 
                            self.master.winfo_height())

    def save(self, event=None):
        """
        Validiert alle Eingaben und setzt die Variable result als Ergebnis.
        """
        datum = self.datum.get()
        projekt = self.projekt.get()
        startzeit = self.startzeit.get()
        endzeit = self.endzeit.get()
        protokoll = self.protokoll.get()
        bezahlt = self.bezahlt.get()
        
        validated_datum = None
        validated_projekt = None
        validated_startzeit = None
        validated_endzeit = None
        validated_protokoll = None
                
        try:
            validated_protokoll = self.validate_protokoll(protokoll)
        except:
                self.protokoll_error["background"] = "red"
                self.protokoll.focus_set()
        
        try:
            validated_endzeit = self.validate_endzeit(startzeit, endzeit)
        except:
                self.endzeit_error["background"] = "red"
                self.endzeit.focus_set()
        
        try:
            validated_startzeit = self.validate_startzeit(startzeit)
        except:
                self.startzeit_error["background"] = "red"
                self.startzeit.focus_set()
                      
        try:
            validated_projekt = self.validate_projekt(projekt)
        except:
                self.projekt_error["background"] = "red"
                self.projekt.focus_set()
        
        try:
            validated_datum = self.validate_datum(datum)
        except:
                self.datum_error["background"] = "red"
                self.datum.focus_set()

        if validated_datum:
            self.datum_error["background"] = "green"

        if validated_projekt:    
            self.projekt_error["background"] = "green"
        
        if validated_startzeit:    
            self.startzeit_error["background"] = "green"
            
        if validated_endzeit:    
            self.endzeit_error["background"] = "green"
            
        if validated_protokoll:
            self.protokoll_error["background"] = "green"
               
        if validated_datum and validated_projekt and validated_startzeit\
                                 and validated_endzeit and validated_protokoll:        
            self.result = (validated_datum, validated_projekt, 
                           validated_startzeit, validated_endzeit, 
                           validated_protokoll, bezahlt, self.t_id)

            self.master.destroy()
           
    def validate_datum(self, datum):
        """
        Valiediert das Datum im Format JJJJ-MM-TT
        """
        datum_pattern = \
        r"^(19|20)\d\d([- /.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$"
        regex = re.compile(datum_pattern)
        valid_date = regex.match(datum)

        if valid_date:
            return datum
        else:
            raise ValueError

    def validate_projekt(self, projekt):
        """
        Valiediert Projekt auf "nicht leer".
        """        
        if projekt.strip():
            return projekt
        else:
            raise ValueError

    def validate_startzeit(self, startzeit):
        """
        Valiediert die Startzeit im Format SS:MM
        """
        startzeit_pattern = r"^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"
        regex = re.compile(startzeit_pattern)
        valid_time = regex.match(startzeit)

        if valid_time:
            return startzeit
        else:
            raise ValueError
        
    def validate_endzeit(self, startzeit, endzeit):
        """
        Valiediert die Endzeit im Format SS:MM
        """
        endzeit_pattern = r"^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"
        regex = re.compile(endzeit_pattern)
        valid_time = regex.match(endzeit)

        if valid_time and startzeit < endzeit:
            return endzeit
        else:
            raise ValueError

    def validate_protokoll(self, protokoll):      
        """
        Valiediert Protokoll auf "nicht leer".
        """ 
        if protokoll.strip():
            return protokoll
        else:
            raise ValueError
