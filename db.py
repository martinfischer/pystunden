"""
Datenbank Modul für pystunden.
"""
import os
import sqlite3

# Appdata und Datenbankdateiname
pystunden_folder = "pystunden"
db_filename = "pystunden-db.db"
# Setzt "folder" je nach Betriebsystem
if os.name == 'nt':   
    folder = os.path.join(os.environ.get('APPDATA'), pystunden_folder)
elif os.name == 'posix':
    folder = os.path.join(os.environ.get('HOME'), "." + pystunden_folder)
else:
    folder = os.path.join(os.getcwd(), pystunden_folder)
# Datenbank Speicherort
db_file = os.path.join(folder, db_filename)
# Falls die Datenbank noch nicht besteht wird hier der Appdata Ordner erstellt
db_is_new = not os.path.exists(db_file)
if db_is_new:
    try:
        os.mkdir(folder)
    except OSError:
        pass

def create_schema():
    """
    Erstellt das Datenbankschema falls die Datei neu erstellt wird.
    """
    with sqlite3.connect(db_file) as con:
        con.execute("""create table stunden(
            id integer primary key,
            datum date not null,
            projekt text not null,
            startzeit time not null, 
            endzeit time not null,
            protokoll text not null,
            bezahlt text default 'Nein')""")

def insert_data(data):
    """
    Fügt einen neuen Datensatz in die Datenbanktabelle stunden ein.
    """
    with sqlite3.connect(db_file) as con:
        con.execute("""insert into stunden
            (datum, projekt, startzeit, endzeit, protokoll, bezahlt)
            values (?, ?, ?, ?, ?, ?)""", data)

def get_all_data():
    """
    Holt alle Datensätze aus der Datenbanktabelle stunden und gibt diese als 
    eine Liste aus Tuppeln zurück.
    """
    data = []
    with sqlite3.connect(db_file) as con:
        for row in con.execute("select * from stunden order by id asc"):
            data.append(row)
    return data

def get_data_by_datum(datum):
    """
    Holt Datensätze per einem Datum aus der Datenbanktabelle stunden und gibt 
    diese als eine Liste aus Tuppeln zurück. 
    
    Das Datum muss hier in Tuppelform wie z.B: ("2010-11-01",) angegeben werden.
    """    
    data = []
    with sqlite3.connect(db_file) as con:
        for row in con.execute("""select * from stunden where datum=?""",
                                datum):
            data.append(row)
    return data

def get_data_by_daten(daten):
    """
    Holt Datensätze zwischen zwei Daten aus der Datenbanktabelle stunden und 
    gibt diese als eine Liste aus Tuppeln zurück. 
    
    Das Datum muss hier in Tuppelform wie z.B: ("2010-11-01", "2010-12-01") 
    angegeben werden.
    """   
    data = []
    with sqlite3.connect(db_file) as con:
        for row in con.execute("""select * from stunden where datum between ? 
                                and ?""", daten):
            data.append(row)
    return data

def get_data_by_daten_and_projekt(daten_und_projekt):
    """
    Holt Datensätze zwischen zwei Daten und per projekt aus der Datenbanktabelle
    stunden und gibt diese als eine Liste aus Tuppeln zurück. 
    
    Das Datum und der Projekt Name muss hier in Tuppelform wie z.B: 
    ("2010-11-01", "2010-12-01", "Projektname") angegeben werden.
    """   
    data = []
    with sqlite3.connect(db_file) as con:
        for row in con.execute("""select * from stunden where datum between ? 
                                and ? and projekt = ?""", daten_und_projekt):
            data.append(row)
    return data    

def get_data_by_projekt(projekt):
    """
    Holt Datensätze zwischen zwei Daten und per projekt aus der Datenbanktabelle
    stunden und gibt diese als eine Liste aus Tuppeln zurück. 
    
    Das Datum und der Projekt Name muss hier in Tuppelform wie z.B: 
    ("2010-11-01", "2010-12-01", "Projektname") angegeben werden.
    """   
    data = []
    with sqlite3.connect(db_file) as con:
        for row in con.execute("""select * from stunden where projekt = ?""", 
                               projekt):
            data.append(row)
    return data   
            
def update_data(data):
    """
    Updates jeweils einen Datenbankeintrag. Erwartet einen vollständigen tuple
    mit validierten Daten.
    """
    with sqlite3.connect(db_file) as con:
        con.execute("""update stunden set
            datum = ?, projekt = ?, startzeit = ?, endzeit = ?, protokoll = ?, 
            bezahlt = ? where id = ?""", data)

def update_data_bezahlt(lst):
    """
    Updates mehr als einen Datenbankeintrag. Erwartet eine Liste mit Tuplen
    mit Ids und dem neuen bezahlt Value.
    """    
    with sqlite3.connect(db_file) as con:
        for data in lst:
            con.execute("""update stunden set
                bezahlt = ? where id = ?""", data)
        
def delete_data(t_id):
    """
    Updates jeweils einen Datenbankeintrag. Erwartet eine bestehende id.
    """    
    with sqlite3.connect(db_file) as con:
        con.execute("""delete from stunden where id = ?""", t_id)
        
def dump_db(filename):
    """
    Dumped die Datenbank im SQL Format und speichert die Datei am angegebenem 
    Dateispeicherort.
    """
    with sqlite3.connect(db_file) as con:
        with open(filename, 'w') as f:
            for line in con.iterdump():
                f.write('{}\n'.format(line))

def import_db(dump):
    """
    Importiert einen exportierten SQL-Dump in die Datenbank.
    """
    with sqlite3.connect(db_file) as con:
        for line in dump:
            con.execute(line)
