"""
Windows cx_freeze script für pystunden

cd zu diesem Ordner - dann "python setup.py build"
"""
from cx_Freeze import setup, Executable

includes = ["sqlite3.dump"]
excludes = []
packages = []
path = []
includefiles = ["README", "LIZENZ", "pystunden.ico"]

build_target = Executable(
    script = "pystunden.py",
    initScript = None,
    base = "Win32GUI",
    targetDir = None,
    targetName = "pystunden.exe",
    compress = True,
    copyDependentFiles = True,
    appendScriptToExe = False,
    appendScriptToLibrary = False,
    icon = "pystunden.ico",
    )

setup(
    name = "pystunden",
    version = "0.2",
    description = "Stunden Bookkeeping",
    author = "Martin Fischer",
    author_email="martin@fischerweb.net",
    options = {"build_exe": {"includes": includes,
                             "excludes": excludes,
                             "packages": packages,
                             "path": path,
                             "include_files": includefiles,
                             }
               },
    executables = [build_target])
