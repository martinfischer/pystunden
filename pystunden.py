"""
--pystunden.py

Ein Stunden Bookkeeping Programm von Martin Fischer.

Sprache: Python3

Features: Erstellung von Stundenarbeitszeiten und deren Bearbeitung, Löschung, 
          Suche nach Datum und Projektname, automatische Berechnung der 
          Stundenanzahl per Eintrag und per Selektion, Export der Ansicht in 
          eine CSV Datei, Export der Datenbank in eine SQL Datei.

Die Datenbankdatei mit dem Namen "pystunden-db.db" wird im SQLITE Format in 
folgendem Ordner abgelegt:
--Windows: %APPDATA%\\pystunden\\pystunden-db.db
--Linux: /home/<user>/.pystunden/pystunden-db.db
--Andere: /<current working directory>/pystunden/pystunden-db.db

Building: cx_freeze build Script ist inkludiert - "python setup.py build"

Lizenz: MIT - siehe Datei LIZENZ
"""
import os
from tkinter import *
from tkinter.ttk import *
from tkinter import messagebox
from tkinter import filedialog
import datetime
import csv
import popup_neu
import popup_bearbeiten
import popup_bearbeiten_bezahlt
import popup_suchen
import db

__version__ = "0.2"

today = datetime.datetime.today()
lizenz = """\
Copyright (c) {} Martin Fischer - martin@fischerweb.net - martinfischer.name
\r\r
Hiermit wird unentgeltlich, jeder Person, die eine Kopie der Software und der 
zugehörigen Dokumentationen (die "Software") erhält, die Erlaubnis erteilt, 
uneingeschränkt zu benutzen, inklusive und ohne Ausnahme, dem Recht, sie zu 
verwenden, kopieren, ändern, fusionieren, verlegen, verbreiten, unterlizenzieren
 und/oder zu verkaufen, und Personen, die diese Software erhalten, diese Rechte 
zu geben, unter den folgenden Bedingungen:
\r\r
Der obige Urheberrechtsvermerk und dieser Erlaubnisvermerk sind in allen Kopien 
oder Teilkopien der Software beizulegen.
\r\r
DIE SOFTWARE WIRD OHNE JEDE AUSDRÜCKLICHE ODER IMPLIZIERTE GARANTIE 
BEREITGESTELLT, EINSCHLIESSLICH DER GARANTIE ZUR BENUTZUNG FÜR DEN 
VORGESEHENEN ODER EINEM BESTIMMTEN ZWECK SOWIE JEGLICHER RECHTSVERLETZUNG, 
JEDOCH NICHT DARAUF BESCHRÄNKT. IN KEINEM FALL SIND DIE AUTOREN ODER 
COPYRIGHTINHABER FÜR JEGLICHEN SCHADEN ODER SONSTIGE ANSPRÜCHE HAFTBAR ZU 
MACHEN, OB INFOLGE DER ERFÜLLUNG EINES VERTRAGES, EINES DELIKTES ODER ANDERS IM 
ZUSAMMENHANG MIT DER SOFTWARE ODER SONSTIGER VERWENDUNG DER SOFTWARE ENTSTANDEN.
""".format(today.year)
lizenz = lizenz.replace("\n","")


class MainWindow(Frame):
    """
    Hauptprogrammfenster
    """
    def __init__(self, master=None):
        Frame.__init__(self, master)
        if os.name == 'nt':
            root.iconbitmap("pystunden.ico")
        root.title("pystunden")
        # Menu
        root.option_add("*tearOff", FALSE)
        self.menu = Menu(root)
        root.config(menu=self.menu)
        self.filemenu = Menu(self.menu)
        self.menu.add_cascade(label="Datei", menu=self.filemenu)
        self.filemenu.add_command(label="Neuer Eintrag", 
                                  command=self.popup_neuer_eintrag)
        self.filemenu.add_command(label="Eintrag Bearbeiten", 
                                  command=self.popup_bearbeiten)
        self.filemenu.add_command(label="Eintrag Löschen", 
                                  command=self.popup_delete)
        self.filemenu.add_command(label="Einträge Suchen", 
                                  command=self.popup_search)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Export Ansicht als CSV Datei",
                                   command=self.export_to_csv)
        self.filemenu.add_command(label="Export Datenbank als SQL-Dump",
                                   command=self.export_db_dump)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Import exportierten SQL-Dump",
                                   command=self.import_db_dump)        
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", command=root.destroy)
        self.helpmenu = Menu(self.menu)
        self.menu.add_cascade(label="Hilfe", menu=self.helpmenu)
        self.helpmenu.add_command(label="Über", command=self.about)
        # Grid    
        self.grid(sticky=W+E+N+S, padx=5, pady=5)       
        # columntree
        self.listframe = Frame(self)
        self.listframe.grid(row=1, column=1, columnspan=8, sticky=W+E+N+S, 
                            padx=1)              
        self.columntree = Treeview(self.listframe, show=["headings"], 
                                   selectmode="extended", height=25)
        self.columntree.grid(row=0,column=0, sticky=W+E+N+S)
        self.columntree["columns"]=("id","datum","projekt","startzeit",
                                    "endzeit","protokoll","bezahlt","stunden")
        self.columntree.heading("id", text="Id")
        self.columntree.column("id", width=40, anchor="center")
        self.columntree.heading("datum", text="Datum")
        self.columntree.column("datum", width=100, anchor="center")
        self.columntree.heading("projekt", text="Projekt")
        self.columntree.column("projekt", width=150)
        self.columntree.heading("startzeit", text="Startzeit")
        self.columntree.column("startzeit", width=100, anchor="center")
        self.columntree.heading("endzeit", text="Endzeit")
        self.columntree.column("endzeit", width=100, anchor="center")
        self.columntree.heading("protokoll", text="Protokoll")
        self.columntree.column("protokoll", width=400)
        self.columntree.heading("bezahlt", text="Bezahlt")
        self.columntree.column("bezahlt", width=75, anchor="center")
        self.columntree.heading("stunden", text="Stunden")
        self.columntree.column("stunden", width=75, anchor="center")
        # Stundenlabel
        self.stunden_variable = StringVar()
        self.stunden_label = Label(self, textvariable=self.stunden_variable, 
                                   width=15)
        self.stunden_label.grid(row=2, column=8)
        self.stunden_variable.set("Stunden:")
        # Scrollbar
        self.vertical_scrollbar = Scrollbar(self.listframe, orient=VERTICAL, 
                                            command=self.columntree.yview)
        self.vertical_scrollbar.grid(row=0, column=1, sticky=N+S, pady=5)
        self.columntree.configure(yscrollcommand=self.vertical_scrollbar.set)
        # Buttons
        self.button_insert = Button(self, text="Neuer Eintrag", width=20, 
                                    command=self.popup_neuer_eintrag)
        self.button_insert.grid(row=11, column=1, padx=10, pady=15)
        self.button_update = Button(self, text="Bearbeiten", width=20, 
                                    command=self.popup_bearbeiten)
        self.button_update.grid(row=11, column=2, padx=10, pady=15)
        self.button_delete = Button(self, text="Löschen", width=20, 
                                    command=self.popup_delete)
        self.button_delete.grid(row=11, column=3, padx=10, pady=15)
        self.button_search = Button(self, text="Suchen", width=20, 
                                    command=self.popup_search)
        self.button_search.grid(row=11, column=4, padx=10, pady=15)       
        self.button_reset = Button(self, text="Reset", width=20, 
                                   command=self.update_tabelle)
        self.button_reset.grid(row=11, column=6, padx=10, pady=15)      
        self.button_search = Button(self, text="Beenden", width=20, 
                                    command=root.destroy)
        self.button_search.grid(row=11, column=8, padx=10, pady=15)
        # Bindings
        self.columntree.bind("<<TreeviewSelect>>", self.selected_stunden)
        self.columntree.bind("<Control-a>", self.select_all)
        # Populiert columntree
        self.update_tabelle()
        # Verhindert Größenänderungen
        root.update()
        root.minsize(root.winfo_width(), root.winfo_height())
        root.maxsize(root.winfo_width(), root.winfo_height())
        
    def about(self):
        """
        Über Popup, zeigt die Lizenz an.
        """
        messagebox.showinfo(title="Über", message="{}".format(lizenz))
        
    def update_tabelle(self):
        """
        Löscht zuerst etwaige columntree Inhalte und populiert columntree dann 
        neu. Setzt auch die unterschiedlichen Hintergrundfarben und resetet das
        Stundenlabel.
        """
        all_items = self.columntree.get_children()
        for item in all_items:
            self.columntree.delete(item)       
        i = 0
        for entry in db.get_all_data():
            entry = list(entry)
            stunden = self.calculate_stunden(entry[3], entry[4])
            entry.append("{:.2f}".format(stunden))
            if i % 2:
                self.columntree.insert("", 0, text=entry[0], values=(entry),
                                       tags=("oddrow"))
            else:
                self.columntree.insert("", 0, text=entry[0], values=(entry),
                                       tags=("evenrow"))
            i+=1           
        self.columntree.tag_configure("oddrow", background="#d0e2f9")
        self.columntree.tag_configure("evenrow", background="#eff2f5")
        self.stunden_variable.set("Stunden:")
            
    def selected_stunden(self, event):
        """
        Berechnet die Stunden der selektierten Einträge und populiert das 
        Stundenlabel mit der Summe.
        """
        selected = self.columntree.selection()
        stunden = []
        for item in selected:
            stunden.append(float(self.columntree.set(item, column="stunden")))
        
        self.stunden_variable.set("Stunden: {:.2f}".format(sum(stunden)))
        
    def select_all(self, event):
        """
        Selektiert alle Einträge nach einem CTRL+A.
        """        
        all_items = self.columntree.get_children()       
        self.columntree.selection_set(all_items)
            
    def popup_neuer_eintrag(self):
        """
        Ruft das Popup Neuer Eintrag und ruft ein columntree Update.
        """
        popup = Toplevel(self)
        d = popup_neu.NeuerEintrag(popup)
        popup.update_idletasks()
        popup.update()
        popup.focus_set()
        popup.grab_set()
        popup.wait_window(popup)
        if d.result:
            db.insert_data(d.result)
        
        self.update_tabelle()
           
    def popup_bearbeiten(self):
        """
        Ruft das Popup Eintrag Bearbeiten und ruft ein columntree Update.
        """        
        selected = self.columntree.selection()
        if len(selected) < 1:
            messagebox.showinfo(title="Fehler", 
                            message="Kein Eintrag zum Bearbeiten ausgewählt!")
        elif len(selected) > 1:
            selected = self.columntree.selection()
            t_ids_bezahlt = []
            for item in selected:
                t_ids_bezahlt.append((self.columntree.set(item, column="id"),
                                self.columntree.set(item, column="bezahlt")))
    
            popup = Toplevel(self)
            d = popup_bearbeiten_bezahlt.EintraegeBearbeiten(t_ids_bezahlt, 
                                                             popup)
            popup.update_idletasks()
            popup.update()
            popup.focus_set()
            popup.grab_set()
            popup.wait_window(popup)
            if d.result:
                db.update_data_bezahlt(d.result)

                self.update_tabelle()

        else:
            t_id = self.columntree.set(selected[0], column="id")
            datum = self.columntree.set(selected[0], column="datum")
            projekt = self.columntree.set(selected[0], column="projekt")
            startzeit = self.columntree.set(selected[0], column="startzeit")
            endzeit = self.columntree.set(selected[0], column="endzeit")
            protokoll = self.columntree.set(selected[0], column="protokoll")
            bezahlt = self.columntree.set(selected[0], column="bezahlt")

            popup = Toplevel(self)
            d = popup_bearbeiten.EintragBearbeiten(t_id, datum, projekt, 
                                                   startzeit, endzeit, 
                                                   protokoll, bezahlt, popup)
            popup.update_idletasks()
            popup.update()
            popup.focus_set()
            popup.grab_set()
            popup.wait_window(popup)
            if d.result:
                db.update_data(d.result)
            
                self.update_tabelle()

    def popup_delete(self):
        """
        Löscht nach einer Nachfrage einen Eintrag aus der Datenbank.
        """
        selected = self.columntree.selection()
        
        if len(selected) < 1:
            messagebox.showinfo(title="Fehler", 
                                message="Kein Eintrag zum Löschen ausgewählt!")
        elif len(selected) > 1:
            messagebox.showinfo(title="Fehler", 
                    message="Bitte nur einen Eintrag zum Löschen auswählen!")     
        else:
            t_id = self.columntree.set(selected[0], column="id")
            
            delete = messagebox.askyesno(
            message="Den Eintrag mit der Id {} wiklich löschen?".format(t_id),
                                icon="question", title="Eintrag löschen?")
            
            if delete:
                t_id = (t_id,)
                db.delete_data(t_id)
                
                self.update_tabelle()
    
    def update_tabelle_after_search(self, data):
        """
        Updatet columntree nach einer Suche mit Suchergebnis "data".
        """
        all_items = self.columntree.get_children()
        for item in all_items:
            self.columntree.delete(item)
        i = 0
        for entry in data:
            entry = list(entry)
            stunden = self.calculate_stunden(entry[3], entry[4])
            entry.append("{:.2f}".format(stunden))
            if i % 2:
                self.columntree.insert("", 0, text=entry[0], values=(entry),
                                       tags=("oddrow"))
            else:
                self.columntree.insert("", 0, text=entry[0], values=(entry),
                                       tags=("evenrow"))
            i+=1           
        self.columntree.tag_configure("oddrow", background="#d0e2f9")
        self.columntree.tag_configure("evenrow", background="#eff2f5")     
        self.stunden_variable.set("Stunden:")   

    def popup_search(self):
        """
        Ruft das Popup Suchen und ruft ein columntree Update mit dem 
        Suchergebnis.
        """   
        popup = Toplevel(self)
        d = popup_suchen.EintragSuchen(popup)
        popup.update_idletasks()
        popup.update()
        popup.focus_set()
        popup.grab_set()
        popup.wait_window(popup)
        if d.result:
            startdatum, enddatum, projekt = d.result
            if startdatum != "None" and enddatum != "None" and projekt != "None": 
                data = db.get_data_by_daten_and_projekt((startdatum, enddatum, 
                                                         projekt))
                self.update_tabelle_after_search(data)
            elif startdatum != "None" and enddatum != "None" and projekt == "None":
                data = db.get_data_by_daten((startdatum, enddatum))
                self.update_tabelle_after_search(data)                
            elif startdatum != "None" and enddatum == "None" and projekt == "None":
                data = db.get_data_by_datum((startdatum,))
                self.update_tabelle_after_search(data)                   
            elif startdatum == "None" and enddatum != "None" and projekt == "None":
                data = db.get_data_by_datum((enddatum,))
                self.update_tabelle_after_search(data)
            elif startdatum == "None" and enddatum == "None" and projekt != "None":
                data = db.get_data_by_projekt((projekt,))
                self.update_tabelle_after_search(data)
                
    def export_db_dump(self):
        """
        asksaveasfilename Dialog für dem Datenbank SQL Dump.
        """
        filename = filedialog.asksaveasfilename(
                                        defaultextension=".sql",
                                        initialdir=os.path.expanduser('~'),
                                        title="Export Datenbank als SQL Datei",
                                        initialfile="pystunden_db",
                                        filetypes=[("SQL Datei", ".sql"),
                                                   ("Alle Dateien", ".*")])
        if filename:
            db.dump_db(filename)

    def export_to_csv(self):
        """
        asksaveasfilename Dialog für den Export der Ansicht als CSV Datei.
        """        
        all_items = self.columntree.get_children()
        all_items = list(all_items)
        all_items.sort()
        data = [("Id", "Datum", "Projekt", "Startzeit", "Endzeit", "Protokoll",
                 "Bezahlt", "Stunden")]
        for item in all_items:
            t_id = self.columntree.set(item, column="id")
            datum = self.columntree.set(item, column="datum")
            projekt = self.columntree.set(item, column="projekt")
            startzeit = self.columntree.set(item, column="startzeit")
            endzeit = self.columntree.set(item, column="endzeit")
            protokoll = self.columntree.set(item, column="protokoll")
            bezahlt = self.columntree.set(item, column="bezahlt")
            stunden = self.columntree.set(item, column="stunden")
            stunden = stunden.replace('.', ',')

            data.append((t_id, datum, projekt, startzeit, endzeit, protokoll,
                        bezahlt, stunden))

        filename = filedialog.asksaveasfilename(
                                        defaultextension=".csv",
                                        initialdir=os.path.expanduser('~'),
                                        title="Export alle Daten als CSV Datei",
                                        initialfile="pystunden_alle_daten",
                                        filetypes=[("CSV Datei", ".csv"),
                                                   ("Alle Dateien", ".*")])        
        if filename:
            with open(filename, "w", newline='') as f:
                writer = csv.writer(f, delimiter=';')
                writer.writerows(data)
                
    def import_db_dump(self):
        """
        Importiert eine voher exportierten SQL-Dump in die Datenbank.
        """
        filename = filedialog.askopenfilename(
                                        defaultextension=".sql",
                                        initialdir=os.path.expanduser('~'),
                                        title="Import SQL-Dump",
                                        initialfile="pystunden_db.sql",
                                        filetypes=[("SQL Datei", ".sql")])         
        if filename:
            with open(filename) as f:
                dump = f.read()
            dump = dump.split("\n")
            dump = dump[9:-2]
            
            db.import_db(dump)
            self.update_tabelle()
            
    def calculate_stunden(self, startzeit, endzeit):
        """
        Berechnet die Stundendifferenz aus 2 Uhrzeitangaben wie z.B: "15:30"
        Es wird eine Float als Stundendifferenz zurückgegeben.
        """
        startzeit_hours, startzeit_minutes = startzeit.split(":")
        enzeit_hours, enzeit_minutes = endzeit.split(":")
            
        startdelta = datetime.timedelta(hours=int(startzeit_hours), 
                                        minutes=int(startzeit_minutes))
        enddelta = datetime.timedelta(hours=int(enzeit_hours), 
                                      minutes=int(enzeit_minutes))
        
        delta = enddelta - startdelta
        stunden = delta.total_seconds() / 3600
    
        return stunden


if __name__ == "__main__":
    # Checkt ob die Datenbankdatei existiert oder erstellt das Schema neu.
    db_is_new = not os.path.exists(db.db_file)
    if db_is_new:
        db.create_schema()
    # Ruft das Hauptprogrammfenster.
    root = Tk()
    app = MainWindow(master=root)
    root.update_idletasks()
    root.update()
    app.mainloop()   
