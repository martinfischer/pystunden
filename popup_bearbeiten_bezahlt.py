"""
Popup Eintrag Bearbeiten für pystunden.
"""
from tkinter import *
from tkinter.ttk import *
from tkinter.simpledialog import Dialog
import os


class EintraegeBearbeiten(Dialog):
    """
    Popupfenster Eintrag Bearbeiten.
    """
    def __init__(self, t_ids_bezahlt,  master=None):
        # Variablen zum Populieren der Radio Buttons
        self.t_ids_bezahlt = t_ids_bezahlt
        # Setzt result None
        self.result = None
        
        self.master = master
        if os.name == 'nt':
            self.master.iconbitmap("pystunden.ico")
        self.master.title("Einträge bearbeiten")
        
        self.l6 = Label(self.master, text="Bezahlt: *", width=20, anchor=E)
        self.l6.grid(row=5, sticky=W+E, padx=5, pady=5)
        
        self.radio_buttons = Label(self.master)
        self.radio_buttons.grid(row=5, column=1, padx=5, pady=5, sticky=W)
        
        self.bezahlt = StringVar(self.master)
        
        self.bezahlt_ja = Radiobutton(self.radio_buttons, text="Ja", 
                                      variable=self.bezahlt, value=str("Ja"))
        self.bezahlt_ja.grid(row=0, column=0, padx=5, pady=5, sticky=W)
        self.bezahlt_nein = Radiobutton(self.radio_buttons, text="Nein", 
                                        variable=self.bezahlt, 
                                        value=str("Nein"))
        self.bezahlt_nein.grid(row=0, column=1, padx=5, pady=5, sticky=W)          
        
        self.button = Button(self.master, text="Speichern", width=20, 
                             command=self.save)
        self.button.grid(row=6, column=0, padx=5, pady=5)        
             
        self.button = Button(self.master, text="Beenden", width=20,
                             command=self.master.destroy)
        self.button.grid(row=6, column=1, sticky=E, padx=5, pady=5)
        
        # Populiert die Radio Buttons. Hier wird ein set verwendet, um den Ver-
        # gleich der "bezahlt" variablen einfach zu machen.
        bezahlt_set = set()
        for id, bezahlt in self.t_ids_bezahlt:
            bezahlt_set.add(bezahlt)
            
        if len(bezahlt_set) == 1:
            for status in bezahlt_set:
                self.bezahlt.set(status)
        else:
            pass
 
        # Bindings
        self.master.bind("<Return>", self.save)
        # Verhindert Größenänderungen
        self.master.update()
        self.master.minsize(self.master.winfo_width(), 
                            self.master.winfo_height())
        self.master.maxsize(self.master.winfo_width(), 
                            self.master.winfo_height())

    def save(self, event=None):
        """
        Setzt die Variable result als Ergebnis.
        """
        bezahlt = self.bezahlt.get()
        
        if bezahlt:
            result = []
            for id, status in self.t_ids_bezahlt:
                result.append((bezahlt, id))
            
            self.result = result

        self.master.destroy()
           
